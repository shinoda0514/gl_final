//(25%)畫出六個房間，並達成以下的條件（編譯要能過且正常執行才算）
//			v每一面牆都必須有貼圖
//			v必須至少有三個房間是使用燈光照明，而且是使用 Per Pixel Lighting
//			v每一個房間中都必須有放置裝飾品(部分必須有貼圖)
//			v鏡頭可以在裡面任意的移動
//(12%)環境與操控
//			v(2%) 鏡頭的移動是根據目前的視角方向
//			v(3%) 不會穿牆
//			v(2%) 會被機關觸動的動態移動光源，使用 Per Pixel Lighting
//					夠靠近豆豆先生的時候環境光會轉動
//			v(2%) 至少三面牆壁上半透明的玻璃
//			v(3%) 能處理不同視角下玻璃能正確顯示
//(6%)圖學相關功能的使用，必須在程式碼中以註解清楚標明
//			v(2%) 針對特定物件實現 Billboards 的功能
//					豆豆先生會一直看著鏡頭
//			v(2%) 使用到  Mipmapped 的功能 （必須有具體的說明在程式碼中）
//					外部大的地板使用Mippmapped
//			v(2%) 有房間使用到 Light Map 的功能 （必須有具體的說明在程式碼中）
//					 最左邊紅色鑽石那間房間使用Light Map
//(3%) 其他你覺得可以展示的技術，包含物理或是數學的運算
//			v在最左邊紅色鑽石的房間待滿5秒就會被傳送回豆豆先生面前並面對豆豆先生
//			v可以破壞房間內的擺設
//				在三面玻璃牆中按K可以弄倒三面玻璃牆並有碎裂效果
//			發射子彈並且在牆壁上留下彈孔
//			以上實現一個即可
//總分：46
//(4%) 創意分數，老師的個人的主觀
//特別要求
//此處的計分必須放在主程式的開頭，並標示出每一個部分是否完成（沒有寫，將不與計分）
//並根據每項完成的部分, 自行加計出總分

//使用說明:
//	W前進、S後退、A往左走、D往右走
//	以滑鼠控制視角
//	進出門要面對門前進
//	在玻璃牆中按K會把三面玻璃牆弄倒
//	在最左邊的房間待滿5秒回被傳送回豆豆先生面前

#include "header/Angel.h"
#include "Common/CQuadMT.h"
#include "Common/CQuad.h"
#include "Common/CWireSphere.h"
#include "Common/CWireCube.h"
#include "Common/CDiamond.h"
#include "Common/CRoom.h"
#include "png_loader.h"

#define SPACE_KEY 32
#define SCREEN_SIZE 600
#define HALF_SIZE SCREEN_SIZE /2 
#define VP_HALFWIDTH  20.0f
#define VP_HALFHEIGHT 20.0f
#define GRID_SIZE 20 // must be an even number

int g_idx=HALF_SIZE,g_idy=HALF_SIZE;
float g_fMoveSpeed=0.1f;
int g_iEyeInRoom=0;//0代表在室外

vec4 g_vAimalPos(0.0,1.0,0.0,1.0);
int g_iSpeed=0;
float fTimeInRoom1=0.0;
float fDegreeForGlass=0.0;
bool bGlassFall=false;
bool bGlassFallen=false;

//for glass
CQuad* g_pGlassLeft,*g_pGlassRight,*g_pGlassBack;
GLuint g_uiGlassLeft,g_uiGlassRight,g_uiGlassBack;

CRoom* room[6];
// For Model View and Projection Matrix
mat4 g_mxModelView(1.0f);
mat4 g_mxProjection;

// For Objects
CQuadMT *g_pFloor,*g_pTop;
GLuint   g_uiDifTexName; 
GLuint   g_uiLghTexName; 
GLuint   g_uiDifTexName2; 
GLuint   g_uiLghTexName2; 
GLuint   g_uiShaderNo;

// For Translucent Object
CQuad  *g_pAimal[1];
GLuint g_uiAimalTexID[3];

// For View Point
GLfloat g_fRadius = 8.0;
GLfloat g_fTheta = 0.0f*DegreesToRadians;
GLfloat g_fPhi = 0.0f*DegreesToRadians;

//point4  g_vEye( g_fRadius*sin(g_fTheta)*sin(g_fPhi), g_fRadius*cos(g_fTheta), g_fRadius*sin(g_fTheta)*cos(g_fPhi),  1.0 );
point4  g_vEye( 0.0, 2.0 , 2.0 , 1.0 );
vec4  g_vEyeDir( sin(g_fTheta)*sin(g_fPhi), cos(g_fTheta), sin(g_fTheta)*cos(g_fPhi),  0.0 );
point4  g_vAt( 0.0, 0.0, 0.0, 1.0 );
vec4    g_vUp( 0.0, 1.0, 0.0, 0.0 );

//----------------------------------------------------------------------------
// Part 2 : for single light source
bool g_bAutoRotating = false;
float g_fElapsedTime = 1;
float g_fLightRadius = 40;
float g_fLightTheta = 0;

float g_fLightR = 1.05f;
float g_fLightG = 1.05f;
float g_fLightB = 1.05f;

structLightSource g_Light1 = {
    color4(g_fLightR, g_fLightG, g_fLightB, 1.0f), // ambient 
	color4(g_fLightR, g_fLightG, g_fLightB, 1.0f), // diffuse
	color4(g_fLightR, g_fLightG, g_fLightB, 1.0f), // specular
    point4(0.0f, 20.0f, 40.0f, 1.0f),   // position
    point4(0.0f, 0.0f, 0.0f, 1.0f),   // halfVector
    vec3(0.0f, 0.0f, 0.0f),			  //spotDirection
	1.0f	,	// spotExponent(parameter e); cos^(e)(phi) 
	45.0f,	// spotCutoff;	// (range: [0.0, 90.0], 180.0)  spot 的照明範圍
	1.0f	,	// spotCosCutoff; // (range: [1.0,0.0],-1.0), 照明方向與被照明點之間的角度取 cos 後, cut off 的值
	1	,	// constantAttenuation	(a + bd + cd^2)^-1 中的 a, d 為光源到被照明點的距離
	0	,	// linearAttenuation	    (a + bd + cd^2)^-1 中的 b
	0		// quadraticAttenuation (a + bd + cd^2)^-1 中的 c
};
CWireSphere *g_pLight;
//----------------------------------------------------------------------------

// Texture 測試用
GLuint g_uiFTexID[3]; // 三個物件分別給不同的貼圖
int g_iTexWidth,  g_iTexHeight;

//----------------------------------------------------------------------------
// 函式的原型宣告
extern void IdleProcess();
int Check()	;//判斷現在眼睛在哪個房間
void GlassFall(float ft);

void init( void )
{
	mat4 mxT;
	vec4 vT, vColor;

	// 讀進貼圖
	//------------------------------Mipmapped----------------------------------------------
	g_uiDifTexName = png_load_SOIL("texture/gravel.png",&g_iTexWidth, &g_iTexHeight, true);//mip map
//	printf("%d  %d\n",g_iTexWidth,g_iTexHeight);
	g_uiLghTexName = png_load_SOIL("texture/lightMap2.png",&g_iTexWidth, &g_iTexHeight, true);
//	printf("%d  %d\n",g_iTexWidth,g_iTexHeight);
	//------------------------------LightMap第一個房間的天花板----------------------------------------------
	g_uiDifTexName2 = png_load_SOIL("texture/blockRedstone.png",&g_iTexWidth, &g_iTexHeight, false);
//	printf("%d  %d\n",g_iTexWidth,g_iTexHeight);
	g_uiLghTexName2 = png_load_SOIL("texture/lightMap1.png",&g_iTexWidth, &g_iTexHeight, false);
//	printf("%d  %d\n",g_iTexWidth,g_iTexHeight);
	

	// 產生所需之 Model View 與 Projection Matrix

	g_mxModelView = LookAt( g_vEye, g_vEye+g_vEyeDir, g_vUp );
	g_mxProjection = Perspective(60.0, (GLfloat)SCREEN_SIZE/(GLfloat)SCREEN_SIZE, 1.0, 1000.0);

	float fSize=10.0;
	// 產生物件的實體
	g_pFloor = new CQuadMT;
	g_pFloor->SetShader(g_mxModelView, g_mxProjection);
	g_pFloor->SetTRSMatrix(Translate(0.0,-0.0001,0.0)*Scale(10.0*fSize,1,10.0f*fSize));
	g_pFloor->SetShadingMode(GOURAUD_SHADING);
	g_pFloor->SetTiling(fSize*8.0f,fSize*8.0f);
	g_pFloor->SetMaterials(vec4(0), vec4(0.85f, 0.85f, 0.85f, 1), vec4(1.0f, 1.0f, 1.0f, 1.0f));
	g_pFloor->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	g_pFloor->SetColor(vec4(0.95f,0.95f,0.95f,1.0f));
//	g_pFloor->SetLightingDisable();
	g_uiShaderNo = g_pFloor->GetShaderHandle();

	//------------------------------LightMap第一個房間的天花板----------------------------------------------
	g_pTop = new CQuadMT;
	g_pTop->SetShader(g_mxModelView, g_mxProjection);
	g_pTop->SetTRSMatrix(Translate(-30.0,10.0-0.000,-10.0)*RotateX(180)*Scale(10.0f,1,10.0f));
	g_pTop->SetShadingMode(GOURAUD_SHADING);
	g_pTop->SetTiling(10.0,10.0);
	g_pTop->SetMaterials(vec4(0), vec4(0.85f, 0.85f, 0.85f, 1), vec4(1.0f, 1.0f, 1.0f, 1.0f));
	g_pTop->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	g_pTop->SetColor(vec4(0.95f,0.95f,0.95f,1.0f));
	g_pTop->SetLightingDisable();
//	g_uiShaderNo = g_pFloor->GetShaderHandle();

	// For g_pAimal1
	g_pAimal[0] = new CQuad;
	g_pAimal[0]->SetShader(g_mxModelView, g_mxProjection);
	mxT = Translate(g_vAimalPos.x, g_vAimalPos.y, g_vAimalPos.z) * RotateX(90) *Scale(2,2,2);
	g_pAimal[0]->SetTRSMatrix(mxT);
	g_pAimal[0]->SetShadingMode(GOURAUD_SHADING);
	// 設定貼圖
	g_pAimal[0]->SetMaterials(vec4(0), vec4(0.85f, 0.85f, 0.85f, 1), vec4(1.0f, 1.0f, 1.0f, 1.0f));
	g_pAimal[0]->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
//	g_pAimal[0]->SetColor(vec4(0.9f, 0.9f, 0.9f, 1.0f));	
//	g_uiAimalTexID[0] = png_load_SOIL("texture/wi_tata.png",&g_iTexWidth, &g_iTexHeight, false);
//	printf("%d  %d\n",g_iTexWidth,g_iTexHeight);
	g_pAimal[0]->SetLightingDisable();
	g_pAimal[0]->LoadPngImageAndSetTextureObject("texture/bean.png");

	// 設定 代表 Light 的 WireSphere ，在這個範例中 燈光是沒有作用的
	g_pLight = new CWireSphere(0.25f, 6, 3);
	g_pLight->SetShader(g_mxModelView, g_mxProjection);
	mxT = Translate(g_Light1.position);
	g_pLight->SetTRSMatrix(mxT);
	g_pLight->SetColor(g_Light1.diffuse);
	g_pLight->SetLightingDisable();

		// For glass wall
	g_pGlassLeft = new CQuad;
	g_pGlassLeft->SetShader(g_mxModelView, g_mxProjection);
	mxT = Translate(-20.0,5.0, 15.0) *RotateY(90)* RotateX(90) *Scale(10,1,10);
	g_pGlassLeft->SetTRSMatrix(mxT);
	g_pGlassLeft->SetShadingMode(GOURAUD_SHADING);
	// 設定貼圖
	g_pGlassLeft->SetMaterials(vec4(0), vec4(0.85f, 0.85f, 0.85f, 1), vec4(1.0f, 1.0f, 1.0f, 1.0f));
	g_pGlassLeft->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
//	g_pAimal[0]->SetColor(vec4(0.9f, 0.9f, 0.9f, 1.0f));	
//	g_uiAimalTexID[0] = png_load_SOIL("texture/wi_tata.png",&g_iTexWidth, &g_iTexHeight, false);
//	printf("%d  %d\n",g_iTexWidth,g_iTexHeight);
	g_pGlassLeft->SetLightingDisable();
	g_pGlassLeft->LoadPngImageAndSetTextureObject("texture/glassred.png");

	g_pGlassBack = new CQuad;
	g_pGlassBack->SetShader(g_mxModelView, g_mxProjection);
	mxT = Translate(-15.0,5.0, 10.0) * RotateX(90) *Scale(10,1,10);
	g_pGlassBack->SetTRSMatrix(mxT);
	g_pGlassBack->SetShadingMode(GOURAUD_SHADING);
	// 設定貼圖
	g_pGlassBack->SetMaterials(vec4(0), vec4(0.85f, 0.85f, 0.85f, 1), vec4(1.0f, 1.0f, 1.0f, 1.0f));
	g_pGlassBack->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
//	g_pAimal[0]->SetColor(vec4(0.9f, 0.9f, 0.9f, 1.0f));	
//	g_uiAimalTexID[0] = png_load_SOIL("texture/wi_tata.png",&g_iTexWidth, &g_iTexHeight, false);
//	printf("%d  %d\n",g_iTexWidth,g_iTexHeight);
	g_pGlassBack->SetLightingDisable();
	g_pGlassBack->LoadPngImageAndSetTextureObject("texture/glassgreen.png");

	g_pGlassRight = new CQuad;
	g_pGlassRight->SetShader(g_mxModelView, g_mxProjection);
	mxT = Translate(-10.0,5.0, 15.0) *RotateY(-90)* RotateX(90) *Scale(10,1,10);
	g_pGlassRight->SetTRSMatrix(mxT);
	g_pGlassRight->SetShadingMode(GOURAUD_SHADING);
	// 設定貼圖
	g_pGlassRight->SetMaterials(vec4(0), vec4(0.85f, 0.85f, 0.85f, 1), vec4(1.0f, 1.0f, 1.0f, 1.0f));
	g_pGlassRight->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
//	g_pAimal[0]->SetColor(vec4(0.9f, 0.9f, 0.9f, 1.0f));	
//	g_uiAimalTexID[0] = png_load_SOIL("texture/wi_tata.png",&g_iTexWidth, &g_iTexHeight, false);
//	printf("%d  %d\n",g_iTexWidth,g_iTexHeight);
	g_pGlassRight->SetLightingDisable();
	g_pGlassRight->LoadPngImageAndSetTextureObject("texture/glassblue.png");

	for(int i=0;i<6;i++)room[i]=new CRoom(i+1,-30.0f+i*15.0f,-10.0f,g_mxModelView,g_mxProjection,g_Light1);
}

//----------------------------------------------------------------------------
void GL_Display( void )
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); // clear the window

	glActiveTexture(GL_TEXTURE0); // select active texture 0
	glBindTexture(GL_TEXTURE_2D, g_uiDifTexName); // 與 Diffuse Map 結合
	glActiveTexture(GL_TEXTURE1); // select active texture 1
	glBindTexture(GL_TEXTURE_2D, WHITENESS); // 與 Light Map 結合

	g_pFloor->Draw();

	glActiveTexture(GL_TEXTURE0); // select active texture 0
	glBindTexture(GL_TEXTURE_2D, g_uiDifTexName2); // 與 Diffuse Map 結合
	glActiveTexture(GL_TEXTURE1); // select active texture 1
	glBindTexture(GL_TEXTURE_2D, g_uiLghTexName2); // 與 Light Map 結合

	g_pTop->Draw();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	

	for(int i=0;i<6;i++)room[i]->Draw();
//	glBindTexture(GL_TEXTURE_2D, 0);
	glEnable(GL_BLEND);  // 設定QAQ2D Texure MappingˊWˋ    有作用 須開啟blend才可用alpha channel
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

//	glBindTexture(GL_TEXTURE_2D, g_uiAimalTexID[0]); 
	if(g_iEyeInRoom==0){
		if(g_vEye.x > -10.0){
			g_pGlassLeft->Draw();
			g_pGlassBack->Draw();
			g_pGlassRight->Draw();
			g_pAimal[0]->Draw();
		}
		else if(g_vEye.x < -20.0){
			g_pAimal[0]->Draw();
			g_pGlassRight->Draw();
			g_pGlassBack->Draw();
			g_pGlassLeft->Draw();
		}
		else if(g_vEye.z<10.0 && g_vEye.x>=-20.0 && g_vEye.x<=-10.0){
			g_pGlassLeft->Draw();
			g_pGlassRight->Draw();
			g_pGlassBack->Draw();
			g_pAimal[0]->Draw();
		}
		else if(g_vEye.z>=10.0 && g_vEye.x>=-20.0 && g_vEye.x<=-10.0){
			g_pAimal[0]->Draw();
			g_pGlassRight->Draw();
			g_pGlassBack->Draw();
			g_pGlassLeft->Draw();
		}
	}
	glDisable(GL_BLEND);	// 關閉 Blending
	glDepthMask(GL_TRUE);	// 開啟對 Z-Buffer 的寫入操作
	

//	g_pLight->Draw();
	glutSwapBuffers();	// 交換 Frame Buffer
}

//----------------------------------------------------------------------------
// Part 2 : for single light source
void UpdateLightPosition(float dt)
{
	mat4 mxT;
	// 每秒繞 Y 軸轉 90 度
	g_fElapsedTime += dt;
	g_fLightTheta = g_fElapsedTime*(float)M_PI_2;
	if( g_fLightTheta >= (float)M_PI*2.0f ) {
		g_fLightTheta -= (float)M_PI*2.0f;
		g_fElapsedTime -= 4.0f;
	}
	g_Light1.position.x = g_fLightRadius * cosf(g_fLightTheta);
	g_Light1.position.z = g_fLightRadius * sinf(g_fLightTheta);
	mxT = Translate(g_Light1.position);
	g_pLight->SetTRSMatrix(mxT);
}
//----------------------------------------------------------------------------

void onFrameMove(float delta)
{
	GlassFall(delta);
	
	if(g_iEyeInRoom==1){
		fTimeInRoom1+=delta;
		if(fTimeInRoom1>=5.0f){
			g_vEye.x=0.0;
			g_vEye.z=2.0;
			g_idx=HALF_SIZE-1;
			g_idy=HALF_SIZE/2;
			g_mxModelView = LookAt( g_vEye, g_vEye+g_vEyeDir, g_vUp );

			// Change ModelView Matrix
			g_pFloor->SetModelViewMatrix(g_mxModelView);
			g_pTop->SetModelViewMatrix(g_mxModelView);
			g_pLight->SetModelViewMatrix(g_mxModelView);

			g_pAimal[0]->SetModelViewMatrix(g_mxModelView);

			for(int i=0;i<6;i++)room[i]->SetModelViewMatrix(g_mxModelView);
			fTimeInRoom1=0.0;

			g_pGlassLeft->SetModelViewMatrix(g_mxModelView);
			g_pGlassBack->SetModelViewMatrix(g_mxModelView);
			g_pGlassRight->SetModelViewMatrix(g_mxModelView);
			
		}
	}
	if( g_bAutoRotating ) { // Part 2 : 重新計算 Light 的位置
		UpdateLightPosition(delta);
	}
	// 如果需要重新計算時，在這邊計算每一個物件的顏色
	g_pFloor->Update(delta, g_Light1);
	g_pTop->Update(delta, g_Light1);

	g_pAimal[0]->Update(delta, g_Light1);

	g_pLight->Update(delta);

	for(int i=0;i<6;i++)room[i]->Update(delta, g_Light1);

	g_pGlassLeft->Update(delta, g_Light1);
	g_pGlassBack->Update(delta, g_Light1);
	g_pGlassRight->Update(delta, g_Light1);

	g_iEyeInRoom=Check();
	printf("%d\n",g_iEyeInRoom);

	glutWarpPointer(HALF_SIZE,HALF_SIZE);
	glutSetCursor(GLUT_CURSOR_NONE);

	if(g_vEye.x>-1.5 && g_vEye.x<1.5 && g_vEye.z>-1.5 && g_vEye.z<1.5)g_bAutoRotating=true;
	else g_bAutoRotating=false;

	GL_Display();

	
}

//----------------------------------------------------------------------------

void Win_Keyboard( unsigned char key, int x, int y )
{
    switch ( key ) {

	case 75://K
	case 107://k
		if(!bGlassFallen && g_vEye.x>-20.0 && g_vEye.x<-10.0 && g_vEye.z>10.0 && g_vEye.z<20.0
			)bGlassFall=true;
		break;

	case  SPACE_KEY:
		g_vEye.x=0.0;
		g_vEye.z=2.0;
		g_idx=HALF_SIZE-1;
		g_idy=HALF_SIZE/2;
		g_mxModelView = LookAt( g_vEye, g_vEye+g_vEyeDir, g_vUp );

		// Change ModelView Matrix
		g_pFloor->SetModelViewMatrix(g_mxModelView);
		g_pTop->SetModelViewMatrix(g_mxModelView);
		g_pLight->SetModelViewMatrix(g_mxModelView);

		g_pAimal[0]->SetModelViewMatrix(g_mxModelView);

		for(int i=0;i<6;i++)room[i]->SetModelViewMatrix(g_mxModelView);
		g_pGlassLeft->SetModelViewMatrix(g_mxModelView);
		g_pGlassBack->SetModelViewMatrix(g_mxModelView);
		g_pGlassRight->SetModelViewMatrix(g_mxModelView);
		break;
	case 79://O加速
	case 111://o
		if(g_fMoveSpeed<1.0)g_fMoveSpeed+=0.1f;
		break;
	case 80://P減速
	case 112://p
		if(g_fMoveSpeed>0.1f)g_fMoveSpeed-=0.1f;
		break;
//----------------------------------------------------------------------------
// Part 2 : for single light source
	case 82: // R key
		if( g_fLightR <= 0.95f ) g_fLightR += 0.05f;
		g_Light1.diffuse.x = g_fLightR;
		g_pLight->SetColor(g_Light1.diffuse);
		break;
	case 114: // r key
		if( g_fLightR >= 0.05f ) g_fLightR -= 0.05f;
		g_Light1.diffuse.x = g_fLightR;
		g_pLight->SetColor(g_Light1.diffuse);
		break;
	case 71: // G key
		if( g_fLightG <= 0.95f ) g_fLightG += 0.05f;
		g_Light1.diffuse.y = g_fLightG;
		g_pLight->SetColor(g_Light1.diffuse);
		break;
	case 103: // g key
		if( g_fLightG >= 0.05f ) g_fLightG -= 0.05f;
		g_Light1.diffuse.y = g_fLightG;
		g_pLight->SetColor(g_Light1.diffuse);
		break;
	case 66: // B key
		if( g_fLightB <= 0.95f ) g_fLightB += 0.05f;
		g_Light1.diffuse.z = g_fLightB;
		g_pLight->SetColor(g_Light1.diffuse);
		break;
	case 98: // b key
		if( g_fLightB >= 0.05f ) g_fLightB -= 0.05f;
		g_Light1.diffuse.z = g_fLightB;
		g_pLight->SetColor(g_Light1.diffuse);
		break;
//----------------------------------------------------------------------------
//--------for motion
	case 65: // A key
	case 97: // a key
		g_vEye+=g_fMoveSpeed*normalize(vec4(sin(g_fTheta)*sin(g_fPhi+M_PI/2),0.0,sin(g_fTheta)*cos(g_fPhi+M_PI/2),0.0));
		if(g_iEyeInRoom==0){
			if( !bGlassFallen &&
				((g_vEye. x>=-19.0 && g_vEye.x<-11.0 && g_vEye.z>9.0 && g_vEye.z<11.0)
				||(g_vEye.x>=-21 && g_vEye.x<=-19.0 && g_vEye.z<=21 && g_vEye.z>=9.0)
				||(g_vEye.x>=-11 && g_vEye.x<=-9 && g_vEye.z<=21 && g_vEye.z>=9.0))
				)g_vEye-=g_fMoveSpeed*normalize(vec4(sin(g_fTheta)*sin(g_fPhi+M_PI/2),0.0,sin(g_fTheta)*cos(g_fPhi+M_PI/2),0.0));
			for(int i=0;i<6;i++){
				if(		(g_vEye.x >= room[i]->GetX()-6.0	)	&&		(g_vEye.x <= room[i]->GetX()+6.0	)
					&&		(g_vEye.z >= room[i]->GetZ()-6.0	)		&&		(g_vEye.z <= room[i]->GetZ()+6.0	))
				{
					g_vEye-=g_fMoveSpeed*normalize(vec4(sin(g_fTheta)*sin(g_fPhi+M_PI/2),0.0,sin(g_fTheta)*cos(g_fPhi+M_PI/2),0.0));					
				}
			}
		}
		else{
			 if(g_vEye.x > room[g_iEyeInRoom-1]->GetX()+4.0 || g_vEye.x < room[g_iEyeInRoom-1]->GetX()-4.0
				|| g_vEye.z >room[g_iEyeInRoom-1]->GetZ()+4.0 || g_vEye.z <room[g_iEyeInRoom-1]->GetZ()-4.0)
					g_vEye-=g_fMoveSpeed*normalize(vec4(sin(g_fTheta)*sin(g_fPhi+M_PI/2),0.0,sin(g_fTheta)*cos(g_fPhi+M_PI/2),0.0));
		}

		g_mxModelView = LookAt( g_vEye, g_vEye+g_vEyeDir, g_vUp );

		// Change ModelView Matrix
		g_pFloor->SetModelViewMatrix(g_mxModelView);
		g_pTop->SetModelViewMatrix(g_mxModelView);
		g_pLight->SetModelViewMatrix(g_mxModelView);

		g_pAimal[0]->SetModelViewMatrix(g_mxModelView);

		for(int i=0;i<6;i++)room[i]->SetModelViewMatrix(g_mxModelView);

		g_pGlassLeft->SetModelViewMatrix(g_mxModelView);
		g_pGlassBack->SetModelViewMatrix(g_mxModelView);
		g_pGlassRight->SetModelViewMatrix(g_mxModelView);
		break;
	case 69: // D key
	case 100: // d key
		g_vEye-=g_fMoveSpeed*normalize(vec4(sin(g_fTheta)*sin(g_fPhi+M_PI/2),0.0,sin(g_fTheta)*cos(g_fPhi+M_PI/2),0.0));

		if(g_iEyeInRoom==0){
			if( !bGlassFallen &&
				((g_vEye. x>=-19.0 && g_vEye.x<-11.0 && g_vEye.z>9.0 && g_vEye.z<11.0)
				||(g_vEye.x>=-21 && g_vEye.x<=-19.0 && g_vEye.z<=21 && g_vEye.z>=9.0)
				||(g_vEye.x>=-11 && g_vEye.x<=-9 && g_vEye.z<=21 && g_vEye.z>=9.0))
				)g_vEye+=g_fMoveSpeed*normalize(vec4(sin(g_fTheta)*sin(g_fPhi+M_PI/2),0.0,sin(g_fTheta)*cos(g_fPhi+M_PI/2),0.0));
			for(int i=0;i<6;i++){
				if(		(g_vEye.x >= room[i]->GetX()-6.0	)	&&		(g_vEye.x <= room[i]->GetX()+6.0	)
					&&		(g_vEye.z >= room[i]->GetZ()-6.0	)		&&		(g_vEye.z <= room[i]->GetZ()+6.0	))
					g_vEye+=g_fMoveSpeed*normalize(vec4(sin(g_fTheta)*sin(g_fPhi+M_PI/2),0.0,sin(g_fTheta)*cos(g_fPhi+M_PI/2),0.0));
			}
		}
		else{
			 if(g_vEye.x > room[g_iEyeInRoom-1]->GetX()+4.0 || g_vEye.x < room[g_iEyeInRoom-1]->GetX()-4.0
				|| g_vEye.z >room[g_iEyeInRoom-1]->GetZ()+4.0 || g_vEye.z <room[g_iEyeInRoom-1]->GetZ()-4.0)
					g_vEye+=g_fMoveSpeed*normalize(vec4(sin(g_fTheta)*sin(g_fPhi+M_PI/2),0.0,sin(g_fTheta)*cos(g_fPhi+M_PI/2),0.0));
		}
		g_mxModelView = LookAt( g_vEye, g_vEye+g_vEyeDir, g_vUp );

		// Change ModelView Matrix
		g_pFloor->SetModelViewMatrix(g_mxModelView);
		g_pTop->SetModelViewMatrix(g_mxModelView);
		g_pLight->SetModelViewMatrix(g_mxModelView);

		g_pAimal[0]->SetModelViewMatrix(g_mxModelView);

		for(int i=0;i<6;i++)room[i]->SetModelViewMatrix(g_mxModelView);

		g_pGlassLeft->SetModelViewMatrix(g_mxModelView);
		g_pGlassBack->SetModelViewMatrix(g_mxModelView);
		g_pGlassRight->SetModelViewMatrix(g_mxModelView);
		break;
		break;
	case 87: // W key
	case 119: // w key
//		g_vEye+=g_fMoveSpeed*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));
		
		g_vEye+=g_fMoveSpeed*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));	
		if(g_iEyeInRoom==0)//如果不在任何一個房間內
		{	
			if( !bGlassFallen &&
				((g_vEye. x>=-19.0 && g_vEye.x<-11.0 && g_vEye.z>9.0 && g_vEye.z<11.0)
				||(g_vEye.x>=-21 && g_vEye.x<=-19.0 && g_vEye.z<=21 && g_vEye.z>=9.0)
				||(g_vEye.x>=-11 && g_vEye.x<=-9 && g_vEye.z<=21 && g_vEye.z>=9.0))
				)g_vEye-=g_fMoveSpeed*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));	
			for(int i=0;i<6;i++){
				if(g_vEye.x<room[i]->GetX()+1.0  &&  g_vEye.x>room[i]->GetX()-1.0
					&& g_vEye.z<room[i]->GetZ()+6.0 && g_vEye.z>room[i]->GetZ() )//從門進去
				{
					g_vEye-=g_fMoveSpeed*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));	
//					g_vEye+=2.1*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));	
					g_vEye=vec4(room[i]->GetX(),g_vEye.y,room[i]->GetZ()+3.5,1.0);
				}
				else if(		(g_vEye.x >= room[i]->GetX()-6.0	)	&&		(g_vEye.x <= room[i]->GetX()+6.0	)
					&&		(g_vEye.z >= room[i]->GetZ()-6.0	)		&&		(g_vEye.z <= room[i]->GetZ()+6.0	))
					g_vEye-=g_fMoveSpeed*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));	
			}
		}
		else //在房間內
		{
			if(g_vEye.x<room[g_iEyeInRoom-1]->GetX()+1.0  &&  g_vEye.x>room[g_iEyeInRoom-1]->GetX()-1.0
				&& g_vEye.z>room[g_iEyeInRoom-1]->GetZ()+4.0 )//從門出來
			{
				g_vEye-=g_fMoveSpeed*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));	
//				g_vEye+=2.1*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));	
				g_vEye=vec4(room[g_iEyeInRoom-1]->GetX(),g_vEye.y,room[g_iEyeInRoom-1]->GetZ()+6.5,1.0);
			}
			else if(g_vEye.x > room[g_iEyeInRoom-1]->GetX()+4.0 || g_vEye.x < room[g_iEyeInRoom-1]->GetX()-4.0
				|| g_vEye.z >room[g_iEyeInRoom-1]->GetZ()+4.0 || g_vEye.z <room[g_iEyeInRoom-1]->GetZ()-4.0)
			{
				g_vEye-=g_fMoveSpeed*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));	
			}
		}

		g_mxModelView = LookAt( g_vEye, g_vEye+g_vEyeDir, g_vUp );

		// Change ModelView Matrix
		g_pFloor->SetModelViewMatrix(g_mxModelView);
		g_pTop->SetModelViewMatrix(g_mxModelView);
		g_pLight->SetModelViewMatrix(g_mxModelView);

		g_pAimal[0]->SetModelViewMatrix(g_mxModelView);

		for(int i=0;i<6;i++)room[i]->SetModelViewMatrix(g_mxModelView);

		g_pGlassLeft->SetModelViewMatrix(g_mxModelView);
		g_pGlassBack->SetModelViewMatrix(g_mxModelView);
		g_pGlassRight->SetModelViewMatrix(g_mxModelView);
		break;
	
	case 83: // S key
	case 115: // s key
		g_vEye-=g_fMoveSpeed*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));
		if(g_iEyeInRoom==0){
			if( !bGlassFallen &&
				((g_vEye. x>=-19.0 && g_vEye.x<-11.0 && g_vEye.z>9.0 && g_vEye.z<11.0)
				||(g_vEye.x>=-21 && g_vEye.x<=-19.0 && g_vEye.z<=21 && g_vEye.z>=9.0)
				||(g_vEye.x>=-11 && g_vEye.x<=-9 && g_vEye.z<=21 && g_vEye.z>=9.0))
				)g_vEye+=g_fMoveSpeed*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));
			for(int i=0;i<6;i++){
				if(		(g_vEye.x >= room[i]->GetX()-6.0	)	&&		(g_vEye.x <= room[i]->GetX()+6.0	)
					&&		(g_vEye.z >= room[i]->GetZ()-6.0	)		&&		(g_vEye.z <= room[i]->GetZ()+6.0	))
					g_vEye+=g_fMoveSpeed*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));
			}
		}
		else{
			 if(g_vEye.x > room[g_iEyeInRoom-1]->GetX()+4.0 || g_vEye.x < room[g_iEyeInRoom-1]->GetX()-4.0
				|| g_vEye.z >room[g_iEyeInRoom-1]->GetZ()+4.0 || g_vEye.z <room[g_iEyeInRoom-1]->GetZ()-4.0)
					g_vEye+=g_fMoveSpeed*normalize(vec4(g_vEyeDir.x,0.0,g_vEyeDir.z,0.0));
		}

		g_mxModelView = LookAt( g_vEye, g_vEye+g_vEyeDir, g_vUp );

		// Change ModelView Matrix
		g_pFloor->SetModelViewMatrix(g_mxModelView);
		g_pTop->SetModelViewMatrix(g_mxModelView);
		g_pLight->SetModelViewMatrix(g_mxModelView);

		g_pAimal[0]->SetModelViewMatrix(g_mxModelView);

		for(int i=0;i<6;i++)room[i]->SetModelViewMatrix(g_mxModelView);

		g_pGlassLeft->SetModelViewMatrix(g_mxModelView);
		g_pGlassBack->SetModelViewMatrix(g_mxModelView);
		g_pGlassRight->SetModelViewMatrix(g_mxModelView);
		break;

    case 033:
		glutIdleFunc( NULL );
		delete g_pFloor;
		delete g_pTop;
		delete g_pLight;
		delete g_pAimal[0];
		for(int i=0;i<6;i++)delete room[i];
        exit( EXIT_SUCCESS );
        break;

    }
}

//----------------------------------------------------------------------------
void Win_Mouse(int button, int state, int x, int y) {
	switch(button) {
		case GLUT_LEFT_BUTTON:   // 目前按下的是滑鼠左鍵
			//if ( state == GLUT_DOWN ) ; 
			break;
		case GLUT_MIDDLE_BUTTON:  // 目前按下的是滑鼠中鍵 ，換成 Y 軸
			//if ( state == GLUT_DOWN ) ; 
			break;
		case GLUT_RIGHT_BUTTON:   // 目前按下的是滑鼠右鍵
			//if ( state == GLUT_DOWN ) ;
			break;
		default:
			break;
	} 
}
//----------------------------------------------------------------------------
void Win_SpecialKeyboard(int key, int x, int y) {

	switch(key) {
		case GLUT_KEY_LEFT:		// 目前按下的是向左方向鍵

			break;
		case GLUT_KEY_RIGHT:	// 目前按下的是向右方向鍵

			break;
		default:
			break;
	}
}

//----------------------------------------------------------------------------
// The passive motion callback for a window is called when the mouse moves within the window while no mouse buttons are pressed.
void Win_PassiveMotion(int x, int y) {
	g_idx+=x-HALF_SIZE;
	g_idy+=y-HALF_SIZE;
//	g_fPhi = (float)-M_PI*(x - HALF_SIZE)/(HALF_SIZE); // 轉換成 g_fPhi 介於 -PI 到 PI 之間 (-180 ~ 180 之間)
//	g_fTheta = (float)M_PI*(float)y/SCREEN_SIZE;
	if(g_idy <=0)g_idy=1;//鎖定視角
	if(g_idy >=HALF_SIZE)g_idy=HALF_SIZE-1;//鎖定視角
	if(g_idx <=-HALF_SIZE)g_idx=HALF_SIZE-1;//防止溢位
	if(g_idx >=3*HALF_SIZE)g_idx=HALF_SIZE;

	g_fPhi = (float)-M_PI*(g_idx)/(HALF_SIZE); // 轉換成 g_fPhi 介於 -PI 到 PI 之間 (-180 ~ 180 之間)
	g_fTheta = (float)M_PI*(g_idy)/(HALF_SIZE);
	
//	printf("%d %d\n",g_idx,g_idy);

	/*
	g_vEye.x = g_fRadius*sin(g_fTheta)*sin(g_fPhi);
	g_vEye.y = g_fRadius*cos(g_fTheta);
	g_vEye.z = g_fRadius*sin(g_fTheta)*cos(g_fPhi);
	*/
	g_vEyeDir.x = sin(g_fTheta)*sin(g_fPhi);
	g_vEyeDir.y = cos(g_fTheta);
	g_vEyeDir.z = sin(g_fTheta)*cos(g_fPhi);

	g_mxModelView = LookAt( g_vEye, g_vEye+g_vEyeDir, g_vUp );
	vec3 vBill=vec3(g_vEye.x-0.0,0.0,g_vEye.z-0.0);
	vBill=normalize(vBill);

	mat4 mxT = Translate(0, 1.0f, 0.0f) *RotateY((float)atan2(vBill.x,vBill.z)/DegreesToRadians)* RotateX(90) *Scale(2,2,2);
	g_pAimal[0]->SetTRSMatrix(mxT);
	
	// Change ModelView Matrix
	g_pFloor->SetModelViewMatrix(g_mxModelView);
	g_pTop->SetModelViewMatrix(g_mxModelView);
	g_pLight->SetModelViewMatrix(g_mxModelView);

	g_pAimal[0]->SetModelViewMatrix(g_mxModelView);
	
	for(int i=0;i<6;i++)room[i]->SetModelViewMatrix(g_mxModelView);

	g_pGlassLeft->SetModelViewMatrix(g_mxModelView);
	g_pGlassBack->SetModelViewMatrix(g_mxModelView);
	g_pGlassRight->SetModelViewMatrix(g_mxModelView);
}

// The motion callback for a window is called when the mouse moves within the window while one or more mouse buttons are pressed.
void Win_MouseMotion(int x, int y) {
	g_idx+=x-HALF_SIZE;
	g_idy+=y-HALF_SIZE;
//	g_fPhi = (float)-M_PI*(x - HALF_SIZE)/(HALF_SIZE); // 轉換成 g_fPhi 介於 -PI 到 PI 之間 (-180 ~ 180 之間)
//	g_fTheta = (float)M_PI*(float)y/SCREEN_SIZE;
	if(g_idy <=0)g_idy=1;//鎖定視角
	if(g_idy >=HALF_SIZE)g_idy=HALF_SIZE-1;//鎖定視角
	if(g_idx <=-HALF_SIZE)g_idx=HALF_SIZE-1;//防止溢位
	if(g_idx >=3*HALF_SIZE)g_idx=HALF_SIZE;

	g_fPhi = (float)-M_PI*(g_idx)/(HALF_SIZE); // 轉換成 g_fPhi 介於 -PI 到 PI 之間 (-180 ~ 180 之間)
	g_fTheta = (float)M_PI*(g_idy)/(HALF_SIZE);
	
//	printf("%d %d\n",g_idx,g_idy);

	/*
	g_vEye.x = g_fRadius*sin(g_fTheta)*sin(g_fPhi);
	g_vEye.y = g_fRadius*cos(g_fTheta);
	g_vEye.z = g_fRadius*sin(g_fTheta)*cos(g_fPhi);
	*/
	g_vEyeDir.x = sin(g_fTheta)*sin(g_fPhi);
	g_vEyeDir.y = cos(g_fTheta);
	g_vEyeDir.z = sin(g_fTheta)*cos(g_fPhi);

	g_mxModelView = LookAt( g_vEye, g_vEye+g_vEyeDir, g_vUp );
	vec3 vBill=vec3(g_vEye.x-0.0,0.0,g_vEye.z-0.0);
	vBill=normalize(vBill);

	mat4 mxT = Translate(0, 1.0f, 0.0f) *RotateY((float)atan2(vBill.x,vBill.z)/DegreesToRadians)* RotateX(90) *Scale(2,2,2);
	g_pAimal[0]->SetTRSMatrix(mxT);
	
	// Change ModelView Matrix
	g_pFloor->SetModelViewMatrix(g_mxModelView);
	g_pLight->SetModelViewMatrix(g_mxModelView);

	g_pAimal[0]->SetModelViewMatrix(g_mxModelView);
	
	for(int i=0;i<6;i++)room[i]->SetModelViewMatrix(g_mxModelView);

	g_pGlassLeft->SetModelViewMatrix(g_mxModelView);
	g_pGlassBack->SetModelViewMatrix(g_mxModelView);
	g_pGlassRight->SetModelViewMatrix(g_mxModelView);
}
//----------------------------------------------------------------------------
void GL_Reshape(GLsizei w, GLsizei h)
{
	glViewport(0, 0, w, h);

	//  產生 projection 矩陣，此處為產生正投影矩陣
	g_mxProjection = Perspective(60.0, (GLfloat)w/(GLfloat)h, 1.0, 1000.0);

	// 設定棋盤的 Projection Matrix
	g_pFloor->SetProjectionMatrix(g_mxProjection);
	g_pTop->SetProjectionMatrix(g_mxProjection);
	g_pLight->SetProjectionMatrix(g_mxProjection); 
	g_pAimal[0]->SetProjectionMatrix(g_mxProjection); 

	for(int i=0;i<6;i++)room[i]->SetProjectionMatrix(g_mxProjection); 

	g_pGlassLeft->SetProjectionMatrix(g_mxProjection); 
	g_pGlassBack->SetProjectionMatrix(g_mxProjection); 
	g_pGlassRight->SetProjectionMatrix(g_mxProjection); 

	glClearColor( 0.0, 0.0, 0.0, 1.0 ); // black background
	glEnable(GL_DEPTH_TEST);
}

//----------------------------------------------------------------------------

int main( int argc, char **argv )
{
    
	glutInit(&argc, argv);
    glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowSize( SCREEN_SIZE, SCREEN_SIZE );

	// If you use freeglut the two lines of code can be added to your application 
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );

    glutCreateWindow("Multi-Texturing");

	// The glewExperimental global switch can be turned on by setting it to GL_TRUE before calling glewInit(), 
	// which ensures that all extensions with valid entry points will be exposed.
	glewExperimental = GL_TRUE; 
    glewInit();  

    init();

	glutMouseFunc(Win_Mouse);
	glutMotionFunc(Win_MouseMotion);
	glutPassiveMotionFunc(Win_PassiveMotion);  
    glutKeyboardFunc( Win_Keyboard );	// 處理 ASCI 按鍵如 A、a、ESC 鍵...等等
	glutSpecialFunc( Win_SpecialKeyboard);	// 處理 NON-ASCI 按鍵如 F1、Home、方向鍵...等等
    glutDisplayFunc( GL_Display );
	glutReshapeFunc( GL_Reshape );
	glutIdleFunc( IdleProcess );
	
    glutMainLoop();
    return 0;
}
int Check()	//判斷現在眼睛在哪個房間
{
	int i=0;
	for(i=0;i<6;i++){
		if(		(g_vEye.x > room[i]->GetX()-5.0	)	&&		(g_vEye.x < room[i]->GetX()+5.0	)
			&&		(g_vEye.z > room[i]->GetZ()-5.0	)		&&		(g_vEye.z < room[i]->GetZ()+5.0	))
			return(i+1);
	}
	return(0);//沒在房間裡面
}
void GlassFall(float ft)
{
	if(bGlassFall){
		fDegreeForGlass+=30.0f * ft;
		
		g_pGlassLeft->SetTRSMatrix(Translate(-20.0,0.0, 15.0)*RotateZ(fDegreeForGlass)*Translate(0.0,5.0, 0.0) *RotateY(90)* RotateX(90) *Scale(10,1,10));
		g_pGlassBack->SetTRSMatrix(Translate(-15.0,0.0, 10.0) *RotateX(-fDegreeForGlass)* Translate(0.0,5.0, 0.0) *RotateX(90) *Scale(10,1,10));
		g_pGlassRight->SetTRSMatrix( Translate(-10.0,0.0, 15.0) *RotateZ(-fDegreeForGlass)*Translate(0.0,5.0, 0.0) *RotateY(-90)* RotateX(90) *Scale(10,1,10));

		if(fDegreeForGlass>89.0){
			bGlassFall=false;
			bGlassFallen=true;
			g_pGlassLeft->LoadPngImageAndSetTextureObject("texture/glassred2.png");
			g_pGlassBack->LoadPngImageAndSetTextureObject("texture/glassgreen2.png");
			g_pGlassRight->LoadPngImageAndSetTextureObject("texture/glassblue2.png");
		}
	}
}
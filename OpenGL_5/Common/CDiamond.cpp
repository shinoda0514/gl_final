#include "CDiamond.h"

CDiamond::CDiamond(float fSize)
{
//	LoadPngImageAndSetTextureObject("texture/Masonry.Unit.Grey.bump.png");
//	LoadPngImageAndSetTextureObject("texture/earth.png");

	int i,j;
	float fCosSin[18][2];
	for(i=0;i<16;i++){
		fCosSin[i][0] = cos(2*PI * i / 16);
		fCosSin[i][1] = sin(2*PI * i / 16);
	}
	fCosSin[16][0]=fCosSin[0][0];
	fCosSin[16][1]=fCosSin[0][1];
	fCosSin[17][0]=fCosSin[1][0];
	fCosSin[17][1]=fCosSin[1][1];
	
	m_iNumVtx = 240;
	m_pPoints = NULL; m_pNormals = NULL; 
	m_pTex1 = NULL;

	m_pPoints  = new vec4[m_iNumVtx];
	m_pNormals = new vec3[m_iNumVtx];
	m_pColors  = new vec4[m_iNumVtx]; 
	m_pTex1     = new vec2[m_iNumVtx];

	//Layer1 8triangles 24points
	 for(i=0,j=0;i<=21;i+=3,j+=2){
		 m_pPoints[i] = point4(fSize *  fCosSin[j][0] * 0.28f ,fSize * 0.15f   , fSize * fCosSin[j][1] * 0.28f, 1.0f );
		 m_pPoints[i+1] = point4( fSize * fCosSin[j+2][0] * 0.28f ,fSize *  0.15f , fSize * fCosSin[j+2][1] * 0.28f, 1.0f );
		 m_pPoints[i+2] = point4( 0.0f ,fSize *  0.15f , 0.0f , 1.0f );
	 }
	//Layer2 8triangles 24points
	 for(i=24,j=1;i<=45;i+=3,j+=2){
		 m_pPoints[i] = point4(fSize *  fCosSin[j+1][0] * 0.28f ,fSize *  0.15f  ,fSize *  fCosSin[j+1][1] * 0.28f, 1.0f );
		 m_pPoints[i+1] = point4( fSize * fCosSin[j-1][0] * 0.28f ,fSize * 0.15f  , fSize *  fCosSin[j-1][1] * 0.28f, 1.0f );
		 m_pPoints[i+2] = point4( fSize * fCosSin[j][0] * 0.35f ,fSize * 0.1f  , fSize *  fCosSin[j][1] * 0.35f, 1.0f );
	 }
	//Layer3 8triangles 24points
	 for(i=48,j=1;i<=69;i+=3,j+=2){
		 m_pPoints[i] = point4( fSize *  fCosSin[j][0]  *0.35f , fSize * 0.1f , fSize *  fCosSin[j][1] * 0.35f, 1.0f );
		 m_pPoints[i+1] = point4( fSize * fCosSin[j+2][0] * 0.35f ,fSize * 0.1f   , fSize * fCosSin[j+2][1] * 0.35f, 1.0f );
		 m_pPoints[i+2] = point4( fSize * fCosSin[j+1][0] * 0.28f ,fSize * 0.15f  ,fSize *  fCosSin[j+1][1] * 0.28f , 1.0f );
	 }
	//Layer4 8triangles 24points
	 for(i=72,j=1;i<=93;i+=3,j+=2){
		 m_pPoints[i] = point4(fSize *  fCosSin[j+2][0] * 0.35f , fSize * 0.1f , fSize *  fCosSin[j+2][1] * 0.35f, 1.0f );
		 m_pPoints[i+1] = point4(fSize *  fCosSin[j][0] * 0.35f ,fSize *  0.1f , fSize *  fCosSin[j][1] * 0.35f, 1.0f );
		 m_pPoints[i+2] = point4( fSize * fCosSin[j+1][0] * 0.5f ,0.0f , fSize *  fCosSin[j+1][1] * 0.5f , 1.0f );
	 }
	//Layer5 16triangles 48points
	 for(i=96,j=0;i<=141;i+=6,j+=2){
		 m_pPoints[i] = point4( fSize * fCosSin[j][0] * 0.5f ,0.0f, fSize *  fCosSin[j][1] * 0.5f  , 1.0f );
		 m_pPoints[i+1] = point4( fSize * fCosSin[j+1][0] * 0.5f , 0.0f ,fSize *  fCosSin[j+1][1] * 0.5f , 1.0f );
		 m_pPoints[i+2] = point4( fSize * fCosSin[j+1][0] * 0.35f , fSize * 0.1f ,fSize *  fCosSin[j+1][1] * 0.35f , 1.0f );

		 m_pPoints[i+3] = point4(fSize *  fCosSin[j+1][0] * 0.5f , 0.0f , fSize * fCosSin[j+1][1] * 0.5f , 1.0f );
		 m_pPoints[i+4] = point4( fSize * fCosSin[j+2][0] * 0.5f , 0.0f ,fSize *  fCosSin[j+2][1] * 0.5f , 1.0f );
		 m_pPoints[i+5] = point4(fSize *  fCosSin[j+1][0] * 0.35f ,fSize *  0.1f , fSize * fCosSin[j+1][1] * 0.35f , 1.0f );
	 }
	//Layer6 16triangles 48points
	 for(i=144,j=0;i<=189;i+=6,j+=2){
		 m_pPoints[i] = point4( fSize * fCosSin[j+1][0] * 0.5f ,0.0f , fSize *  fCosSin[j+1][1] * 0.5f , 1.0f );
		 m_pPoints[i+1] = point4( fSize * fCosSin[j][0] * 0.5f , 0.0f ,fSize *  fCosSin[j][1] * 0.5f , 1.0f );
		 m_pPoints[i+2] = point4( fSize * fCosSin[j+1][0] * 0.17f , fSize * -0.3f , fSize * fCosSin[j+1][1] * 0.17f , 1.0f );

		 m_pPoints[i+3] = point4( fSize * fCosSin[j+2][0] * 0.5f , 0.0f ,fSize *  fCosSin[j+2][1] * 0.5f , 1.0f );
		 m_pPoints[i+4] = point4(fSize *  fCosSin[j+1][0] * 0.5f , 0.0f ,fSize *  fCosSin[j+1][1] * 0.5f , 1.0f );
		 m_pPoints[i+5] = point4( fSize * fCosSin[j+1][0] * 0.17f ,fSize *  -0.3f ,fSize *  fCosSin[j+1][1] * 0.17f , 1.0f );
	 }
	//Layer7 8triangles 24points
	 for(i=192,j=1;i<=213;i+=3,j+=2){
		 m_pPoints[i] = point4( fSize * fCosSin[j][0] * 0.17f ,fSize *  -0.3f , fSize * fCosSin[j][1] * 0.17f , 1.0f );
		 m_pPoints[i+1] = point4(fSize *  fCosSin[j+2][0] * 0.17f ,fSize *  -0.3f , fSize * fCosSin[j+2][1] * 0.17f , 1.0f );
		 m_pPoints[i+2] = point4(fSize *  fCosSin[j+1][0] * 0.5f , 0.0f , fSize * fCosSin[j+1][1] * 0.5f , 1.0f );
	 }
	//Layer8 8triangles 24points
	 for(i=216,j=1;i<=237;i+=3,j+=2){
		 m_pPoints[i] = point4( fSize * fCosSin[j+2][0] * 0.17f ,fSize *  -0.3f ,fSize *  fCosSin[j+2][1] * 0.17f , 1.0f );
		 m_pPoints[i+1] = point4( fSize * fCosSin[j][0] * 0.17f ,fSize *  -0.3f , fSize * fCosSin[j][1] * 0.17f , 1.0f );
		 m_pPoints[i+2] = point4( 0.0f ,fSize *  -0.45f , 0.0f , 1.0f );
	 }

	 //產生normal
	 for(i=0;i<m_iNumVtx;i+=3){
		vec4 u = m_pPoints[i+1] - m_pPoints[i];
		vec4 v = m_pPoints[i+2] - m_pPoints[i];
		vec3 normal = normalize( cross(u, v) );
		m_pNormals[i]=- normal;
		m_pNormals[i+1]=- normal;
		m_pNormals[i+2]= -normal;
	 }

	 for(i=0;i<m_iNumVtx;i+=3){
		m_pTex1[i] = vec2(0.0f, 0.0f);
		m_pTex1[i+1] = vec2(0.5f, 1.0f);
		m_pTex1[i+2] = vec2(1.0f, 0.0f);
	 }


	// 預設將所有的面都設定成灰色
	for( int i = 0 ; i < m_iNumVtx ; i++ ) m_pColors[i] = vec4(-1.0f,-1.0f,-1.0f,1.0f);

#ifdef LIGHTING_WITHCPU
	// Default Set shader's name
	SetShaderName("vsLighting_CPU.glsl", "fsLighting_CPU.glsl");
#else // lighting with GPU
#ifdef PERVERTEX_LIGHTING
	SetShaderName("vsLighting_GPU.glsl", "fsLighting_GPU.glsl");
#else
	SetShaderName("vsPerPixelLighting.glsl", "fsPerPixelLighting.glsl");
#endif
#endif  

	// Create and initialize a buffer object ，將此部分的設定移入 SetShader 中
//	CreateBufferObject();

	// 設定材質
	SetMaterials(vec4(0), vec4(0.5f, 0.5f, 0.9f, 1), vec4(1.0f, 1.0f, 1.0f, 1.0f));
	SetKaKdKsShini(0, 0.8f, 0.2f, 1);
}


void CDiamond::Draw()
{
//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);  // Change to wireframe mode
	DrawingSetShader();
	glBindTexture(GL_TEXTURE_2D, m_uiTexObject[0]); 
	glDrawArrays( GL_TRIANGLES, 0, m_iNumVtx );
	glBindTexture(GL_TEXTURE_2D, 0); 
//	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // Return to solid mode
}


void CDiamond::DrawW()
{
	DrawingWithoutSetShader();
	glBindTexture(GL_TEXTURE_2D, m_uiTexObject[0]); 
	glDrawArrays( GL_TRIANGLES, 0, m_iNumVtx );
	glBindTexture(GL_TEXTURE_2D, 0); 
}


void CDiamond::RenderWithFlatShading(vec4 vLightPos, color4 vLightI)
{
	// 以每一個面的三個頂點計算其重心，以該重心作為顏色計算的點頂
	// 根據 Phong lighting model 計算相對應的顏色，並將顏色儲存到此三個點頂
	// 因為每一個平面的頂點的 Normal 都相同，所以此處並沒有計算此三個頂點的平均 Normal
	/*
	vec4 vCentroidP;
	for( int i = 0 ; i < m_iNumVtx ; i += 3 ) {
		// 計算三角形的重心
		vCentroidP = (m_pPoints[i] + m_pPoints[i+1] + m_pPoints[i+2])/3.0f;
		m_pColors[i] = m_pColors[i+1] = m_pColors[i+2] = PhongReflectionModel(vCentroidP, m_pNormals[i], vLightPos, vLightI);
	}
	glBindBuffer( GL_ARRAY_BUFFER, m_uiBuffer );
	glBufferSubData( GL_ARRAY_BUFFER, sizeof(vec4)*m_iNumVtx+sizeof(vec3)*m_iNumVtx, sizeof(vec4)*m_iNumVtx, m_pColors ); // vertcies' Color
	*/
}


void CDiamond::RenderWithGouraudShading(vec4 vLightPos, color4 vLightI)
{
	vec4 vCentroidP;
	for( int i = 0 ; i < m_iNumVtx ; i += 1 ) {
		m_pColors[i]   =  PhongReflectionModel(m_pPoints[i], m_pNormals[i], vLightPos,  vLightI);
		m_pColors[i+2] =  PhongReflectionModel(m_pPoints[i+2], m_pNormals[i+2], vLightPos,  vLightI);
		m_pColors[i+1] = PhongReflectionModel(m_pPoints[i+1], m_pNormals[i+1], vLightPos,  vLightI);
	}
	glBindBuffer( GL_ARRAY_BUFFER, m_uiBuffer );
	glBufferSubData( GL_ARRAY_BUFFER, sizeof(vec4)*m_iNumVtx+sizeof(vec3)*m_iNumVtx, sizeof(vec4)*m_iNumVtx, m_pColors ); // vertcies' Color
}

// 此處所給的 vLightPos 必須是世界座標的確定絕對位置
void CDiamond::Update(float dt, point4 vLightPos, color4 vLightI)
{
#ifdef LIGHTING_WITHCPU
	if( m_bMVUpdated  ) { // Model View 的相關矩陣內容有更動
		m_mxMVFinal = m_mxModelView * m_mxTRS;
		m_mxMV3X3Final = mat3(
			m_mxMVFinal._m[0].x,  m_mxMVFinal._m[1].x, m_mxMVFinal._m[2].x,
			m_mxMVFinal._m[0].y,  m_mxMVFinal._m[1].y, m_mxMVFinal._m[2].y,
			m_mxMVFinal._m[0].z,  m_mxMVFinal._m[1].z, m_mxMVFinal._m[2].z);

#ifdef GENERAL_CASE
		m_mxITMV = InverseTransposeMatrix(m_mxMVFinal); 
#endif

		m_bMVUpdated = false;
	}
	if( m_iMode == FLAT_SHADING ) RenderWithFlatShading(vLightPos, vLightI);
	else RenderWithGouraudShading(vLightPos, vLightI);

#else // Lighting With GPU
	if( m_bMVUpdated  ) {
		m_mxMVFinal = m_mxModelView * m_mxTRS;
		m_bMVUpdated = false;
	}
	m_vLightInView = m_mxModelView * vLightPos;		// 將 Light 轉換到鏡頭座標再傳入
	// 算出 AmbientProduct DiffuseProduct 與 SpecularProduct 的內容
	m_AmbientProduct  = m_Material.ka * m_Material.ambient  * vLightI;
	m_DiffuseProduct  = m_Material.kd * m_Material.diffuse  * vLightI;
	m_SpecularProduct = m_Material.ks * m_Material.specular * vLightI;
#endif
}


void CDiamond::Update(float dt, const structLightSource &LightSource)
{
#ifdef LIGHTING_WITHCPU
	if( m_bMVUpdated  ) { // Model View 的相關矩陣內容有更動
		m_mxMVFinal = m_mxModelView * m_mxTRS;
		m_mxMV3X3Final = mat3(
			m_mxMVFinal._m[0].x,  m_mxMVFinal._m[1].x, m_mxMVFinal._m[2].x,
			m_mxMVFinal._m[0].y,  m_mxMVFinal._m[1].y, m_mxMVFinal._m[2].y,
			m_mxMVFinal._m[0].z,  m_mxMVFinal._m[1].z, m_mxMVFinal._m[2].z);

#ifdef GENERAL_CASE
		m_mxITMV = InverseTransposeMatrix(m_mxMVFinal); 
#endif

		m_bMVUpdated = false;
	}
	if( m_iMode == FLAT_SHADING ) RenderWithFlatShading(LightSource.position, LightSource.diffuse);
	else RenderWithGouraudShading(LightSource.position, LightSource.diffuse);

#else // Lighting With GPU
	if( m_bMVUpdated  ) {
		m_mxMVFinal = m_mxModelView * m_mxTRS;
		m_bMVUpdated = false;
	}
	m_vLightInView = m_mxModelView * LightSource.position;		// 將 Light 轉換到鏡頭座標再傳入
	// 算出 AmbientProduct DiffuseProduct 與 SpecularProduct 的內容
	m_AmbientProduct  = m_Material.ka * m_Material.ambient  * LightSource.ambient;
	m_DiffuseProduct  = m_Material.kd * m_Material.diffuse  * LightSource.diffuse;
	m_SpecularProduct = m_Material.ks * m_Material.specular * LightSource.specular;
#endif

}

void CDiamond::Update(float dt)
{
	if( m_bMVUpdated  ) { // Model View 的相關矩陣內容有更動
		m_mxMVFinal = m_mxModelView * m_mxTRS;
		m_mxITMV = InverseTransposeMatrix(m_mxMVFinal); 
	}
}
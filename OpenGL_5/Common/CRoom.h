#ifndef CROOM_H
#define CROOM_H
#include "../header/Angel.h"
#include "CShape.h"
#include "CShapeMT.h"
#include "CQuad.h"
#include "CQuadMT.h"
#include "CWall.h"
#include "CDiamond.h"
#include "../png_loader.h"
#include "TypeDefine.h"



class CRoom
{
private:
	int m_iNum;//房間編號
	float m_iX;
	float m_iZ;
	float fD;//牆壁正反面的距離
	structLightSource m_Light;
	structLightSource m_OuterLight;
	CWall* m_pWallLeft;
	CWall* m_pWallLeftOut;
	CWall* m_pWallRight;
	CWall* m_pWallRightOut;
	CWall* m_pWallBack;
	CWall* m_pWallBackOut;
	CWall* m_pWallFront;
	CWall* m_pWallFrontOut;
	CWall* m_pWallFloor;
	CWall* m_pWallTop;
	CDiamond* m_pDiamond;
	
	

public:
	CRoom(int num,float x,float z,mat4 &modelview,mat4 &projection,structLightSource &light);
	~CRoom();
	void Update(float dt,structLightSource &light); // 不計算光源的照明
	void SetModelViewMatrix(mat4 &mat);
	void SetProjectionMatrix(mat4 &mat);
	void SetShader(mat4 &modelview,mat4 &projection);
	void SetLightingDisable();
	void Draw();
	void DrawW();
	void SetTRSMatrix(mat4 &mat);
	void SetOuterLight(structLightSource &light);
	float GetX();
	float GetZ();
};




#endif
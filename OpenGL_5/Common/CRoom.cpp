#include "CRoom.h"



CRoom::CRoom(int num,float x,float z,mat4 &modelview,mat4 &projection,structLightSource &light)
{
	m_OuterLight=light;

	m_iNum=num;
	m_iX=x;
	m_iZ=z;

	fD=0.001;//牆壁正反面的距離

	m_pWallLeft=new CWall;
	m_pWallLeftOut=new CWall;
	m_pWallRight=new CWall;
	m_pWallRightOut=new CWall;
	m_pWallBack=new CWall;
	m_pWallBackOut=new CWall;
	m_pWallFront=new CWall;
	m_pWallFrontOut=new CWall;
	m_pWallFloor=new CWall;
	m_pWallTop=new CWall;
	m_pDiamond=new CDiamond(2.0);

	

	switch (m_iNum)
	{
	case 1:
		m_pWallLeftOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallLeftOut->SetTiling(8,8);
		m_pWallRightOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallRightOut->SetTiling(8,8);
		m_pWallBackOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallBackOut->SetTiling(8,8);
		m_pWallFrontOut->LoadPngImageAndSetTextureObject("texture/WallFront.png");
		m_pDiamond->LoadPngImageAndSetTextureObject("texture/blockRedstone.png");

		m_pWallLeft->LoadPngImageAndSetTextureObject("texture/Black.png");
		m_pWallRight->LoadPngImageAndSetTextureObject("texture/Black.png");
		m_pWallBack->LoadPngImageAndSetTextureObject("texture/Black.png");
		m_pWallFront->LoadPngImageAndSetTextureObject("texture/Black.png");
		m_pWallFloor->LoadPngImageAndSetTextureObject("texture/Black.png");
		m_pWallTop->LoadPngImageAndSetTextureObject("texture/Black.png");		

		m_pWallLeft->SetLightingDisable();
		m_pWallRight->SetLightingDisable();
		m_pWallBack->SetLightingDisable();
		m_pWallFront->SetLightingDisable();
		m_pWallFloor->SetLightingDisable();
		m_pWallTop->SetLightingDisable();
		break;
	case 2:
		m_pWallLeftOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallLeftOut->SetTiling(8,8);
		m_pWallRightOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallRightOut->SetTiling(8,8);
		m_pWallBackOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallBackOut->SetTiling(8,8);
		m_pWallFrontOut->LoadPngImageAndSetTextureObject("texture/WallFront.png");
		m_pDiamond->LoadPngImageAndSetTextureObject("texture/dragonEgg.png");
		
		m_pWallLeft->LoadPngImageAndSetTextureObject("texture/dropper_front.png");
		m_pWallLeft->SetTiling(8,8);
		m_pWallRight->LoadPngImageAndSetTextureObject("texture/dropper_front.png");
		m_pWallRight->SetTiling(8,8);
		m_pWallBack->LoadPngImageAndSetTextureObject("texture/dropper_front.png");
		m_pWallBack->SetTiling(8,8);
		m_pWallFront->LoadPngImageAndSetTextureObject("texture/WallFront.png");
		m_pWallFloor->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallFloor->SetTiling(8,8);
		m_pWallTop->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallTop->SetTiling(8,8);
		break;
	case 3:
		m_pWallLeftOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallLeftOut->SetTiling(8,8);
		m_pWallRightOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallRightOut->SetTiling(8,8);
		m_pWallBackOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallBackOut->SetTiling(8,8);
		m_pWallFrontOut->LoadPngImageAndSetTextureObject("texture/WallFront.png");
		m_pDiamond->LoadPngImageAndSetTextureObject("texture/blockGold.png");

		m_pWallLeft->LoadPngImageAndSetTextureObject("texture/gold_ore.png");
		m_pWallLeft->SetTiling(8,8);
		m_pWallRight->LoadPngImageAndSetTextureObject("texture/gold_ore.png");
		m_pWallRight->SetTiling(8,8);
		m_pWallBack->LoadPngImageAndSetTextureObject("texture/gold_ore.png");
		m_pWallBack->SetTiling(8,8);
		m_pWallFront->LoadPngImageAndSetTextureObject("texture/WallFront.png");
		m_pWallFloor->LoadPngImageAndSetTextureObject("texture/gravel.png");
		m_pWallFloor->SetTiling(8,8);
		m_pWallTop->LoadPngImageAndSetTextureObject("texture/gold_ore.png");
		m_pWallTop->SetTiling(8,8);
		break;
	case 4:
		m_pWallLeftOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallLeftOut->SetTiling(8,8);
		m_pWallRightOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallRightOut->SetTiling(8,8);
		m_pWallBackOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallBackOut->SetTiling(8,8);
		m_pWallFrontOut->LoadPngImageAndSetTextureObject("texture/WallFront.png");
		m_pDiamond->LoadPngImageAndSetTextureObject("texture/blockDiamond.png");

		m_pWallLeft->LoadPngImageAndSetTextureObject("texture/diamond_ore.png");
		m_pWallLeft->SetTiling(8,8);
		m_pWallRight->LoadPngImageAndSetTextureObject("texture/diamond_ore.png");
		m_pWallRight->SetTiling(8,8);
		m_pWallBack->LoadPngImageAndSetTextureObject("texture/diamond_ore.png");
		m_pWallBack->SetTiling(8,8);
		m_pWallFront->LoadPngImageAndSetTextureObject("texture/WallFront.png");
		m_pWallFloor->LoadPngImageAndSetTextureObject("texture/gravel.png");
		m_pWallFloor->SetTiling(8,8);
		m_pWallTop->LoadPngImageAndSetTextureObject("texture/diamond_ore.png");
		m_pWallTop->SetTiling(8,8);
		break;
	case 5:
		m_pWallLeftOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallLeftOut->SetTiling(8,8);
		m_pWallRightOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallRightOut->SetTiling(8,8);
		m_pWallBackOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallBackOut->SetTiling(8,8);
		m_pWallFrontOut->LoadPngImageAndSetTextureObject("texture/WallFront.png");
		m_pDiamond->LoadPngImageAndSetTextureObject("texture/gold.png");

		m_pWallLeft->LoadPngImageAndSetTextureObject("texture/destroy_9.png");
		m_pWallLeft->SetTiling(8,8);
		m_pWallRight->LoadPngImageAndSetTextureObject("texture/destroy_9.png");
		m_pWallRight->SetTiling(8,8);
		m_pWallBack->LoadPngImageAndSetTextureObject("texture/destroy_9.png");
		m_pWallBack->SetTiling(8,8);
		m_pWallFront->LoadPngImageAndSetTextureObject("texture/WallFront.png");
		m_pWallFloor->LoadPngImageAndSetTextureObject("texture/destroy_9.png");
		m_pWallFloor->SetTiling(8,8);
		m_pWallTop->LoadPngImageAndSetTextureObject("texture/destroy_9.png");
		m_pWallTop->SetTiling(8,8);
		break;
	case 6:
		m_pWallLeftOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallLeftOut->SetTiling(8,8);
		m_pWallRightOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallRightOut->SetTiling(8,8);
		m_pWallBackOut->LoadPngImageAndSetTextureObject("texture/dispenser_front_vertical.png");
		m_pWallBackOut->SetTiling(8,8);
		m_pWallFrontOut->LoadPngImageAndSetTextureObject("texture/WallFront.png");
		m_pDiamond->LoadPngImageAndSetTextureObject("texture/blockLapis.png");

		m_pWallLeft->LoadPngImageAndSetTextureObject("texture/ice.png");
		m_pWallLeft->SetTiling(8,8);
		m_pWallRight->LoadPngImageAndSetTextureObject("texture/ice.png");
		m_pWallRight->SetTiling(8,8);
		m_pWallBack->LoadPngImageAndSetTextureObject("texture/ice.png");
		m_pWallBack->SetTiling(8,8);
		m_pWallFront->LoadPngImageAndSetTextureObject("texture/WallFront.png");
		m_pWallFloor->LoadPngImageAndSetTextureObject("texture/gravel.png");
		m_pWallFloor->SetTiling(8,8);
		m_pWallTop->LoadPngImageAndSetTextureObject("texture/ice.png");
		m_pWallTop->SetTiling(8,8);
		break;

	default:
		
		break;
	}
	
	SetShader(modelview,projection);
	
	m_pWallLeft->SetShadingMode(GOURAUD_SHADING);
	m_pWallLeftOut->SetShadingMode(GOURAUD_SHADING);
	m_pWallRight->SetShadingMode(GOURAUD_SHADING);
	m_pWallRightOut->SetShadingMode(GOURAUD_SHADING);
	m_pWallBack->SetShadingMode(GOURAUD_SHADING);
	m_pWallBackOut->SetShadingMode(GOURAUD_SHADING);
	m_pWallFront->SetShadingMode(GOURAUD_SHADING);
	m_pWallFrontOut->SetShadingMode(GOURAUD_SHADING);
	m_pWallFloor->SetShadingMode(GOURAUD_SHADING);
	m_pWallTop->SetShadingMode(GOURAUD_SHADING);
	m_pDiamond->SetShadingMode(GOURAUD_SHADING);

	m_pWallLeft->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	m_pWallLeftOut->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	m_pWallRight->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	m_pWallRightOut->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	m_pWallBack->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	m_pWallBackOut->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	m_pWallFront->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	m_pWallFrontOut->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	m_pWallFloor->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	m_pWallTop->SetKaKdKsShini(0, 0.8f, 0.5f, 1);
	m_pDiamond->SetKaKdKsShini(0, 0.8f, 0.5f, 1);

//	SetLightingDisable();

	mat4 mxT=Translate(m_iX,0.0,m_iZ);
	
	m_pWallLeft->SetTRSMatrix(mxT*Translate(-5.0+fD,5.0,0.0)*RotateY(90)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallLeftOut->SetTRSMatrix(mxT*Translate(-5.0,5.0,0.0)*RotateY(-90)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallRight->SetTRSMatrix(mxT*Translate(5.0-fD,5.0,0.0)*RotateY(-90)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallRightOut->SetTRSMatrix(mxT*Translate(5.0,5.0,0.0)*RotateY(90)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallBack->SetTRSMatrix(mxT*Translate( 0.0,5.0,-5.0+fD)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallBackOut->SetTRSMatrix(mxT*Translate( 0.0,5.0,-5.0)*RotateY(180)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallFront->SetTRSMatrix(mxT*Translate( 0.0,5.0,5.0-fD)*RotateY(180)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallFrontOut->SetTRSMatrix(mxT*Translate( 0.0,5.0,5.0)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallFloor->SetTRSMatrix(mxT*Scale(10.0,1.0,10.0));
	m_pWallTop->SetTRSMatrix(mxT*Translate( 0.0,10.0,0.0)*RotateX(180)*Scale(10.0,1.0,10.0));
	m_pDiamond->SetTRSMatrix(mxT*Translate( 0.0,5.0,0.0));

	float fLightR = 1.05f;
	float fLightG = 1.05f;
	float fLightB = 1.05f;

	m_Light.ambient = color4(0.1f, 0.1f, 0.1f, 1.0f); // ambient 
	m_Light.diffuse = color4(fLightR, fLightG, fLightB, 1.0f); // diffuse
	m_Light.specular = color4(fLightR, fLightG, fLightB, 1.0f); // specular
	if(m_iNum!=1)m_Light.position = point4(m_iX+2.5, 2.0f, m_iZ, 1.0f);   // position
	else m_Light.position = point4(m_iX, 1.0f, m_iZ, 1.0f);   // position
	m_Light.halfVector = point4(0.0f, 0.0f, 0.0f, 1.0f);   // halfVector
	m_Light.spotDirection = vec3(0.0f, 0.0f, 0.0f);			  //spotDirection
	m_Light.spotExponent = 1.0f	;	// spotExponent(parameter e); cos^(e)(phi) 
	m_Light.spotCutoff = 45.0f;	// spotCutoff;	// (range: [0.0, 90.0], 180.0)  spot 的照明範圍
	m_Light.spotCosCutoff = 1.0f;		// spotCosCutoff; // (range: [1.0,0.0],-1.0), 照明方向與被照明點之間的角度取 cos 後, cut off 的值
	m_Light.constantAttenuation = 1;	// constantAttenuation	(a + bd + cd^2)^-1 中的 a, d 為光源到被照明點的距離
	m_Light.linearAttenuation = 0;		// linearAttenuation	    (a + bd + cd^2)^-1 中的 b
	m_Light.quadraticAttenuation = 0;		// quadraticAttenuation (a + bd + cd^2)^-1 中的 c
	
}

CRoom::~CRoom()
{
	delete m_pWallLeft;
	delete m_pWallLeftOut;
	delete m_pWallRight;
	delete m_pWallRightOut;
	delete m_pWallBack;
	delete m_pWallBackOut;
	delete m_pWallFront;
	delete m_pWallFrontOut;
	delete m_pWallFloor;
	delete m_pWallTop;
	delete m_pDiamond;
}

void CRoom::Update(float dt,structLightSource &light)
{
	m_OuterLight=light;
	
	m_pWallLeft->Update(dt, m_Light);
	m_pWallRight->Update(dt, m_Light);
	m_pWallBack->Update(dt, m_Light);
	m_pWallFront->Update(dt, m_Light);
	m_pWallLeftOut->Update(dt, m_OuterLight);
	m_pWallRightOut->Update(dt, m_OuterLight);
	m_pWallBackOut->Update(dt, m_OuterLight);
	m_pWallFrontOut->Update(dt, m_OuterLight);
	m_pWallFloor->Update(dt, m_Light);
	m_pWallTop->Update(dt, m_Light);
	
	m_pDiamond->Update(dt, m_Light);
}
void CRoom::SetModelViewMatrix(mat4 &mat)
{
	m_pWallLeft->SetModelViewMatrix(mat);
	m_pWallRight->SetModelViewMatrix(mat);
	m_pWallBack->SetModelViewMatrix(mat);
	m_pWallFront->SetModelViewMatrix(mat);
	m_pWallFloor->SetModelViewMatrix(mat);
	m_pWallLeftOut->SetModelViewMatrix(mat);
	m_pWallRightOut->SetModelViewMatrix(mat);
	m_pWallBackOut->SetModelViewMatrix(mat);
	m_pWallFrontOut->SetModelViewMatrix(mat);
	m_pWallTop->SetModelViewMatrix(mat);
	m_pDiamond->SetModelViewMatrix(mat);
}
void CRoom::SetProjectionMatrix(mat4 &mat)
{
	m_pWallLeft->SetProjectionMatrix(mat);
	m_pWallRight->SetProjectionMatrix(mat);
	m_pWallBack->SetProjectionMatrix(mat);
	m_pWallFront->SetProjectionMatrix(mat);
	m_pWallFloor->SetProjectionMatrix(mat);
	m_pWallLeftOut->SetProjectionMatrix(mat);
	m_pWallRightOut->SetProjectionMatrix(mat);
	m_pWallBackOut->SetProjectionMatrix(mat);
	m_pWallFrontOut->SetProjectionMatrix(mat);
	m_pWallTop->SetProjectionMatrix(mat);
	m_pDiamond->SetProjectionMatrix(mat);
}
void CRoom::SetShader(mat4 &modelview,mat4 &projection)
{
	m_pWallLeft->SetShader(modelview,projection);
	m_pWallRight->SetShader(modelview,projection);
	m_pWallBack->SetShader(modelview,projection);
	m_pWallFront->SetShader(modelview,projection);
	m_pWallFloor->SetShader(modelview,projection);
	m_pWallLeftOut->SetShader(modelview,projection);
	m_pWallRightOut->SetShader(modelview,projection);
	m_pWallBackOut->SetShader(modelview,projection);
	m_pWallFrontOut->SetShader(modelview,projection);
	m_pWallTop->SetShader(modelview,projection);
	m_pDiamond->SetShader(modelview,projection);
}
void CRoom::SetLightingDisable()
{
	m_pWallLeft->SetLightingDisable();
	m_pWallRight->SetLightingDisable();
	m_pWallBack->SetLightingDisable();
	m_pWallFront->SetLightingDisable();
	m_pWallFloor->SetLightingDisable();
	m_pWallLeftOut->SetLightingDisable();
	m_pWallRightOut->SetLightingDisable();
	m_pWallBackOut->SetLightingDisable();
	m_pWallFrontOut->SetLightingDisable();
	m_pWallTop->SetLightingDisable();
	m_pDiamond->SetLightingDisable();
}
void CRoom::Draw()
{
	//glEnable(GL_BLEND);  // 設定2D Texure Mapping 有作用 須開啟blend才可用alpha channel
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glDepthMask(GL_FALSE);

	m_pWallLeft->Draw();
	m_pWallRight->Draw();
	m_pWallBack->Draw();
	m_pWallFloor->Draw();
	m_pWallFront->Draw();
	m_pWallLeftOut->Draw();
	m_pWallRightOut->Draw();
	m_pWallBackOut->Draw();
	m_pWallFrontOut->Draw();
	m_pWallTop->Draw();
	//glDisable(GL_BLEND);	// 關閉 Blending
	//glDepthMask(GL_TRUE);	
	
	m_pDiamond->Draw();
	
}
void CRoom::DrawW()
{
	m_pWallLeft->DrawW();
	m_pWallRight->DrawW();
	m_pWallBack->DrawW();
//	m_pWallFront->DrawW();
	m_pWallFloor->DrawW();
	m_pWallLeftOut->DrawW();
	m_pWallRightOut->DrawW();
	m_pWallBackOut->DrawW();
	m_pWallFrontOut->DrawW();
	m_pWallTop->DrawW();
	m_pDiamond->DrawW();
}
void CRoom::SetTRSMatrix(mat4 &mat)
{
	m_pWallLeft->SetTRSMatrix(mat*Translate(-5.0+fD,5.0,0.0)*RotateY(90)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallLeftOut->SetTRSMatrix(mat*Translate(-5.0,5.0,0.0)*RotateY(-90)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallRight->SetTRSMatrix(mat*Translate(5.0-fD,5.0,0.0)*RotateY(-90)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallRightOut->SetTRSMatrix(mat*Translate(5.0,5.0,0.0)*RotateY(90)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallBack->SetTRSMatrix(mat*Translate( 0.0,5.0,-5.0+fD)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallBackOut->SetTRSMatrix(mat*Translate( 0.0,5.0,-5.0)*RotateY(180)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallFront->SetTRSMatrix(mat*Translate( 0.0,5.0,5.0-fD)*RotateY(180)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallFrontOut->SetTRSMatrix(mat*Translate( 0.0,5.0,5.0)*RotateX(90)*Scale(10.0,1.0,10.0));
	m_pWallFloor->SetTRSMatrix(mat*Scale(10.0,1.0,10.0));
	m_pWallTop->SetTRSMatrix(mat*Translate( 0.0,10.0,0.0)*RotateX(180)*Scale(10.0,1.0,10.0));
	m_pDiamond->SetTRSMatrix(mat*Translate( 0.0,5.0,0.0));
}
void CRoom::SetOuterLight(structLightSource &light)
{
	m_OuterLight=light;
}
float CRoom::GetX()
{
	return(m_iX);
}
float CRoom::GetZ()
{
	return(m_iZ);
}
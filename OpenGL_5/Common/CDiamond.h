#ifndef CDIAMOND_H
#define CDIAMOND_H
#include "../header/Angel.h"
#include "CShape.h"

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

#define PI 3.14159265359f

class CDiamond : public CShape
{
private:

public:
	CDiamond(float fSize);
	~CDiamond(){};

	void Update(float dt, point4 vLightPos, color4 vLightI );
	void Update(float dt, const structLightSource &LightSource);
	void Update(float dt); // 不計算光源的照明

	void RenderWithFlatShading(point4 vLightPos, color4 vLightI);
	void RenderWithGouraudShading(point4 vLightPos, color4 vLightI);
	void Draw();
	void DrawW();
};

#endif